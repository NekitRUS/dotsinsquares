Координаты двух точек вводятся с клавиатуры. Случайным образом генерируются n квадратов (n задается с клавиатуры).
 
Определяется, сколько квадратов содержат обе данные точки.
 
![Безымянный.png](https://bitbucket.org/repo/7Ldo9K/images/1542626660-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)
![Безымянный4.png](https://bitbucket.org/repo/7Ldo9K/images/164643080-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B94.png)
![Безымянный2.png](https://bitbucket.org/repo/7Ldo9K/images/3948873693-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B92.png)
![Безымянный3.png](https://bitbucket.org/repo/7Ldo9K/images/598373968-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B93.png)