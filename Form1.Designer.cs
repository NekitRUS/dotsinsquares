﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.about = new System.Windows.Forms.Button();
            this.input_data = new System.Windows.Forms.Button();
            this.draw_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // about
            // 
            this.about.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.about.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.about.Location = new System.Drawing.Point(475, 10);
            this.about.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.about.Name = "about";
            this.about.Size = new System.Drawing.Size(106, 36);
            this.about.TabIndex = 0;
            this.about.Text = "About";
            this.about.UseVisualStyleBackColor = true;
            this.about.Click += new System.EventHandler(this.about_Click);
            // 
            // input_data
            // 
            this.input_data.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.input_data.Location = new System.Drawing.Point(9, 10);
            this.input_data.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.input_data.Name = "input_data";
            this.input_data.Size = new System.Drawing.Size(112, 36);
            this.input_data.TabIndex = 1;
            this.input_data.Text = "Ввести данные";
            this.input_data.UseVisualStyleBackColor = true;
            this.input_data.Click += new System.EventHandler(this.input_data_Click);
            // 
            // draw_button
            // 
            this.draw_button.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.draw_button.Enabled = false;
            this.draw_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.draw_button.Location = new System.Drawing.Point(203, 10);
            this.draw_button.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.draw_button.Name = "draw_button";
            this.draw_button.Size = new System.Drawing.Size(174, 36);
            this.draw_button.TabIndex = 2;
            this.draw_button.Text = "Начертить/перечертить объекты";
            this.draw_button.UseVisualStyleBackColor = true;
            this.draw_button.Click += new System.EventHandler(this.draw_button_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.BackColor = System.Drawing.SystemColors.Info;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(348, 486);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 40);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 534);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.draw_button);
            this.Controls.Add(this.input_data);
            this.Controls.Add(this.about);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Курсовая работа";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button about;
        private System.Windows.Forms.Button input_data;
        public System.Windows.Forms.Button draw_button;
        private System.Windows.Forms.Label label1;
    }
}

