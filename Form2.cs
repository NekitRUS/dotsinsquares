﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        //Обработчик загрузки формы: Вывод задания на элемент управления "метка"
        private void Form2_Load(object sender, EventArgs e)
        {
            label1.Font = new Font("Arial", 12, FontStyle.Bold);
            label1.Text = "Курсовая работа.\n\nСтудент: Титов Н.А.\nГруппа: И8-12-3";
            label2.Font = new Font("Times New Roman", 10);
            label2.Text = "Задание:\n№23. Имеются две точки с координатами (a,b) и (c,d).\nЗадать координаты вершин для n квадратов (x1(i),y1(i)-x2(i),y2(i)).\nОпределить, сколько квадратов содержат обе данные точки.\nПримечание: обе точки лежат в квадрате, если\nx1(i)<a<x2(i) и y1(i)<by2(i) и x1(i)<c<x2(i) и y1(i)<d<y2(i).";
        }
    }
}
