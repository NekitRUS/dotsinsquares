﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }
        /*Обработчик клика по кнопке "ОК": при успешном проходе проверки на незаполненные поля
         * и вложенной проверки на ввод недопустимых символов инициализирует открытые переменные главной формы значениями,
         * введенными пользователем с клавиатуры; в противном случае показывается сообщение с ошибкой*/
        private void send_data_Click(object sender, EventArgs e)
        {
            if ((square_num.Text != "") && (x1.Text != "") && (x2.Text != "") && (y1.Text != "") && (y2.Text != ""))
            {
                string digits = square_num.Text + x1.Text + x2.Text + y1.Text + y2.Text;
                bool ok = true;
                for (int i = 0; i < digits.Length; i++)
                {
                    if (char.IsDigit(digits[i]) || digits[i] == ',' || digits[i] == '-')
                    {
                    }
                    else
                        ok = false;

                }
                if (ok)
                {
                    Form1 main = Application.OpenForms["Form1"] as Form1;
                    main.draw_button.Enabled = true;
                    main.a = Convert.ToSingle(x1.Text);
                    main.b = Convert.ToSingle(y1.Text);
                    main.c = Convert.ToSingle(x2.Text);
                    main.d = Convert.ToSingle(y2.Text);
                    main.n = Convert.ToInt32(square_num.Text);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Не допускается ввод букв!\nДля ввода вещественного числа используйте запятую.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Необходимо заполнить все поля!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
