﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Объявление публичных переменных для хранения кол-ва квадратов и координат точек
        public int n;
        public float a, b, c, d;
        /*Описание структуры "квадрат", которая имеет 4 открытых поля,
         * соответствующих координатам вершин, конструктор и булевый метод*/
        struct square
        {
            public int x1, y1, x2, y2;
            public square (int x1, int y1, int x2, int y2)
            {
                this.x1 = x1;
                this.y1 = y1;
                this.x2 = x2;
                this.y2 = y2;
            }
            //Метод возвращает истину, если точка с передаваемыми методу координатами лежит внутри квадрата
            public bool inside(float a1, float b1)
            {
                if ((x1 < a1) && (a1 < x2) && (y1 < b1) && (b1 < y2))
                    return true;
                else
                    return false;
            }
        }
        //Метод для генерации случайным образом одного цвета из пяти
        private Color get_color()
        {
            Random rnd = new Random();
            Color out_color = new Color();
            switch (rnd.Next(1, 5))
            {
                case 1:
                    out_color = Color.LimeGreen;
                    break;
                case 2:
                    out_color = Color.Blue;
                    break;
                case 3:
                    out_color = Color.SeaGreen;
                    break;
                case 4:
                    out_color = Color.Yellow;
                    break;
                case 5:
                    out_color = Color.Sienna;
                    break;
            }
            return out_color;
        }
        /*Метод для вычерчивания квадрата, который принимает в кач. аргументов структуру "квадрат"
         * и объект, содержащий данные для рисования*/
        private void draw_squares(square kvadr, PaintEventArgs e)
        {
            Graphics drawer = e.Graphics;
            Pen pen_for_kvadr = new Pen(get_color(), 3f);
            drawer.DrawRectangle(pen_for_kvadr, kvadr.x1, -kvadr.y2, (kvadr.x2 - kvadr.x1), (kvadr.y2 - kvadr.y1));
        }
        //Метод рисования точек, введенных пользователем с клавиатуры
        private void draw_dots(PaintEventArgs e)
        {
            Graphics drawer = e.Graphics;
            SolidBrush brush_for_dots = new SolidBrush(Color.Red);
            drawer.FillEllipse(brush_for_dots, (a - 10 / 2), -(b + 10 / 2), 10, 10);
            drawer.FillEllipse(brush_for_dots, (c - 10 / 2), -(d + 10 / 2), 10, 10);
        }
        /*Метод вычерчивания осей координат, представляющих собой две пересекающиеся линии, проведенные через середину формы,
        * центра координат сдвигом на половину габаритных размеров формы, а также подписи осей*/
        private void draw_axis(PaintEventArgs e)
        {
            Graphics drawer = e.Graphics;
            Pen pen_for_axies = new Pen(Color.Black, 2f);
            Font font_for_axies = new Font("Arial", 12f);
            drawer.DrawLine(pen_for_axies, (this.Width) / 2f, (about.Height + 20f), (this.Width / 2f), (this.Height - 40f));
            drawer.DrawLine(pen_for_axies, 10f, (this.Height / 2f), (this.Width - 20f), (this.Height / 2f));
            drawer.TranslateTransform((this.Width / 2f), (this.Height / 2f));
            drawer.DrawString("X", font_for_axies, Brushes.Black, (this.Width / 2f - 40f), 0f);
            drawer.DrawString("Y", font_for_axies, Brushes.Black, 0f, -(this.Height / 2f - (about.Height + 20f)));
            drawer.DrawString("0;0", font_for_axies, Brushes.Black, 0f, 0f);
        }
        //Обработчик клика по кнопке "About": создается и отображается новая форма
        private void about_Click(object sender, EventArgs e)
        {
            Form2 aboutProgramm = new Form2();
            aboutProgramm.ShowDialog();
        }
        //Обработчик клика по кнопке "Ввести данные": создается и отображается новая форма
        private void input_data_Click(object sender, EventArgs e)
        {
            Form3 data = new Form3();
            data.ShowDialog();
        }
        //Обработчик клика по кнопке "Начертить/перечертить объекты"
        private void draw_button_Click(object sender, EventArgs e)
        {
            this.Refresh(); //Принудительное обновление формы(очистка от предыдущих чертежей)
            //Создание экземпляра класса, предоставляющего средства для рисования
            PaintEventArgs g = new PaintEventArgs(this.CreateGraphics(), ClientRectangle); 
            int result = 0;
            Random koord = new Random();
            square[] kvadrati = new square[n]; //Выделение памяти под массив квадратов 
            draw_axis(g); //Вызов метода вычерчивания осей
            draw_dots(g); //Вызов метода рисующего точки
            /*Заполнение полей каждого элемента массива через конструктор с использованием генератора случайных чисел
             * а также вызов проверяющего метода на принажлежность точки квадрату*/
            for (int i = 0; i < n; i++)
            {
                //Инициализация начальных координат квадрата в пределах границ формы
                int x_s = koord.Next(-this.Width / 2, this.Width / 2);
                int y_s = koord.Next(-this.Height / 2, this.Height / 2);
                int length = koord.Next(10, this.Height / 2); //Генерация длины стороны квадрата случачайным образом
                kvadrati[i] = new square(x_s, y_s, (x_s + length), (y_s + length));
                draw_squares(kvadrati[i], g);//Вызов метода вычерчивания квадрата
                /*Проверка условия [с последующим увеличением счетчика квадратов] попадания точки в квадрат
                 * через вызов экземплярного метода*/
                if (kvadrati[i].inside(a, b) && kvadrati[i].inside(c, d))
                    result++;
            }
            //Вывод результата на элемент управления "метка"
            string result_output = "Обе точки содержат " + result.ToString() + " квадратов";
            label1.Text = result_output;
            label1.Visible = true;
        }
    }
}